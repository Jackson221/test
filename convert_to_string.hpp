/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Convert types to string with recursive templating
 *
 * Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include "variadic_util/variadic_util.hpp" 
#include <string>

namespace test
{

#ifdef NO_TEST
	template<typename T>
	inline std::string convert_to_string(T in)
	{
		return "";
	}
#else

	inline std::string convert_to_string(std::string_view data) { return std::string("\"")+std::string(data)+"\"";/*string_manip::sprintf<_internal_string_wrap>(data);*/ }
	inline std::string convert_to_string(const char* data) {  return std::string("\"")+data+"\"";/*string_manip::sprintf<_internal_string_wrap>(data);*/ }
	inline std::string convert_to_string(bool data) { return data? "true" : "false"; }
	
	/*!
	 * Convertable but not literally a string
	 */
	template<typename T>
	concept CDefaultConvertableToString = requires(T t)
	{
		{ std::to_string(t) } -> std::same_as<std::string>;
		requires !std::is_same_v<T,std::string>;
	};
	template<typename T>
	concept OptionalLike = requires(T t)
	{
		{ t.has_value() } -> std::same_as<bool>;
	};
	template<typename T>
	concept ContainerLike = requires(T t)
	{
		{ t.begin() } -> std::same_as<typename T::iterator>;
		requires !std::is_same_v<T, std::string>;
	};
	template<typename T>
	concept TupleLike = requires(T t)
	{
		{ t.~T() };
		{ std::get<0>(t) };
	};
	template<typename T>
	concept MemberFunctionConvertible = requires(T const & t)
	{
		{ t.unit_test_to_string() } -> std::same_as<std::string>;
	};


	template<CDefaultConvertableToString T>
	std::string convert_to_string(T const & data);
	template<OptionalLike T>
	std::string convert_to_string(T const & data);
	template<TupleLike T>
	std::string convert_to_string(T const & data);
	template<ContainerLike T>
	std::string convert_to_string(T const & data);
	template<MemberFunctionConvertible T>
	std::string convert_to_string(T const & data) { return data.unit_test_to_string(); }

	/*!
	 * User-templatable function to convert a type to string
	 */
	template<CDefaultConvertableToString T>
	std::string convert_to_string(T const & data)
	{
		return std::to_string(data);
	}

	inline std::string convert_to_string(std::string data)
	{
		return data;
	}
	

	constexpr char _internal_bracket_wrap[] = "{%s%}";	
	template<OptionalLike T>
	std::string convert_to_string(T const & data)
	{
		return data.has_value()? ("{"+convert_to_string(data.value())+"}") : "{}";
	}
	template<TupleLike T>
	std::string convert_to_string(T const & data)
	{
		std::string proto_result;
		variadic_util::for_each_element_in_variadic(data, [&](auto in){proto_result += convert_to_string(in)+",";});
		return std::string("{") + proto_result.substr(0,std::max(size_t(0),proto_result.size()-1)) + "}";
	}
	template<ContainerLike T>
	std::string convert_to_string(T const & data)
	{
		std::string returnval = "";
		if constexpr( std::is_same_v<typename T::value_type,char>)
		{
			for (char& element : data)
			{
				returnval += element;	
			}
		}
		else
		{
			for (auto& element : data)
			{
				returnval += convert_to_string(element);
				returnval += ",";
			}
		}
		return "{"+(returnval.substr(0,std::max(size_t(0),returnval.size()-1)))+"}";
	}
	

#endif //NO_TEST
	//list of strings which are 'illegal' and thus must be escaped
	inline std::vector<std::tuple<char,std::string>> illegal_to_escape =
	{
		{'\0',"[NULL]"},
		{'\n',"[NL]"},
		{'"',"\\\""}
	};
	inline std::string escape_string(std::string in)
	{
		std::string out;
		out.reserve(in.size());
		size_t last_insert_end = 0;
		for(size_t i = 0; i != in.size(); i++)
		{
			for(auto& seq : illegal_to_escape)
			{
				if (in[i] == std::get<0>(seq))
				{
					out.append(in.begin()+last_insert_end,in.begin()+i);	
					out.append(std::get<1>(seq));
					last_insert_end = i+1;
				}
			}
		}
		out.append(in.begin()+last_insert_end,in.end());
		return out;
	}

}
