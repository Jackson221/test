# Test module

The test module is a unit testing module written for C++20. It is possible to substitute it with a stub for C++17 builds by defining `NO_TEST`. 

Usage is simple. Define a function taking in a `test::instance& inst`, then call `inst.set_name(std::string)`. From there, you need to be aware of 3 macros:

| Macro | Usage
| --- | ---
| TEST_0 | Used for no-variable tests using the == operator 
| TEST_1 | Used for single-variable tests using any operator.
| TEST_2 | Used for 2-variable tests using any operator

Use these in conjunction with `inst.test` to easily run tests:

```
inst.test(TEST_0(some_function,"some output it should give"));
inst.test(TEST_1(some_variable, some_variable > 27,true));
inst.test(TEST_2(some_variable,another_variable, some_variable==another_variable,true));
```
And finally, you can write messages using `inst.inform(std::string)`. A good way to use these is to label information about the variables you're testing, or what the test success / failure means. 

# Pretty Printing

The test module will automatically convert the value of variables to string to show their value at test time. So, if you provide a `std::vector<std::optional<int>>`, the test module will pretty print it for you -- for example, `{{42},{32},{}}` for a vector of 42, 32, and nullopt. You can overload `convert_to_string` for your own custom datatypes and they will be pretty printed. Just make sure to define your overload before including the main test.hpp

# Design Notes

Behind the scenes, the unit test module tries to do as much type inference as possible. The module will try to convert your value before comparing it if it cannot be compared directly (e.x. C-string and std::string). It will also try to deduce what typeless brackets mean so that `std::optional<int>(42)` and `{42}` can be successfully compared.

# Dependancies

Depends upon Module3D's variadic_util module and logger module

# Use

Simply add the files to your build system, and provide the parent directory of all depandancies as an include flag (i.e. -I"src/" )

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.

