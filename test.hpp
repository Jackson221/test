/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * An unit-testing library 
 *
 * Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <string>
#include <functional>
#include <vector>
#include <cstddef>
#include <type_traits>
#include <optional>
#include <stdexcept>
#include <cstdio>

#include "logger/logger.hpp"

#include "test/convert_to_string.hpp"


/*!
 * Ideas:
 *
 * * Test streams. So, if you wanted a test to output let's say 27,65,28 you could do something like teststream << 27 << 65 << 28 and then provide the final teststream produced by the test
 *   and compare. Additionally, something like teststream << ( out_of_order << (block << 27 << 65 << 28) << (block << 77 << 44 << 32)) could work for threading
 */

namespace test
{
	//experiment with concepts -- in unit test library code since application can ship w/o it
	



	
	

	namespace
	{
		std::string _internal_pass_color = "\033[1;92m";	
		std::string _internal_fail_color = "\033[1;91m";	


#define _INTERNAL_RESET_STR "\033[0m"

		std::string _internal_test_format_2_variable = "%s[%s] TRIED: %s WITH: %s==%s, %s==%s | WANTED %s%s" _INTERNAL_RESET_STR;
		std::string _internal_test_format_1_variable = "%s[%s] TRIED: %s WITH: %s==%s | WANTED %s%s" _INTERNAL_RESET_STR;
		std::string _internal_test_format = "%s[%s] TRIED: %s | WANTED %s%s" _INTERNAL_RESET_STR;
		std::string _internal_got_msg = ", BUT GOT: %s";

#undef _INTERNAL_RESET_STR

		template<typename ... T>
		std::string stringsprintf(std::string format, T ... t)
		{
			char buf[20480];
			sprintf(buf, format.c_str(), t.c_str()...);
			return std::string(buf);
		}
	}

#ifdef NO_TEST

	template<typename T, typename Y>
	inline bool compare_and_convert_if_needed(const T& t, const Y& y)
	{
		return false;
	}
#else // NO_TEST

	template<typename T, typename Y>
	concept Comparable = requires(T t, Y y)
	{
		{ t == y } -> std::same_as<bool>;
	};

	template<typename T, typename Y>
	concept ConvertibleLeftToRight = requires(T t, Y y)
	{
		{ static_cast<Y>(t) } -> std::same_as<Y>;
	};

	/*!
	 * This will compare 2 values of maybe different types. They must fit ONE:
	 *  * Give an operator== between the two types
	 *  * One type be comparable to itself, and...
	 *  	% Provide a way to convert t to y
	 *  	% Provide a way to convert y to t
	 *  * Both types implicitly convertible to a type that defines an operator== on itself
	 */
	template<typename T, typename Y>
		requires(Comparable<T,Y>)
	inline bool compare_and_convert_if_needed(const T& t, const Y& y)
	{
		return t == y;
	}
	template<typename T, typename Y>
		requires (!Comparable<T,Y> && ConvertibleLeftToRight<T,Y> && !std::is_same_v<T,Y>)
	inline bool compare_and_convert_if_needed(const T& t, const Y& y)
	{
		return compare_and_convert_if_needed(static_cast<Y>(t),y);
	}
	template<typename T, typename Y>
		requires (!Comparable<T,Y> && ConvertibleLeftToRight<Y,T> && !std::is_same_v<T,Y>)
	inline bool compare_and_convert_if_needed(const T& t, const Y& y)
	{
		return compare_and_convert_if_needed(t,static_cast<T>(y));
	}

#endif


	class instance
	{
		friend void _internal_run_tests(std::vector<instance>& instances, std::function<void(std::string)> puts_function, std::function<void(instance&)> this_test);
		public:
			std::function<void(std::string)> puts_function = [](std::string in){std::puts(in.c_str());};
			enum class message_data
			{
				pass,
				fail,
				info,
				crit,
				SIZE
			};
		protected:
			std::vector<std::string> _held_messages = {};
			std::vector<message_data> _message_metadata = {};
			std::optional<std::string> _name  = {};

		private:
			void push_back_test(std::string message, bool result_is_pass)
			{
				_held_messages.push_back(message);
				_message_metadata.push_back(result_is_pass? message_data::pass : message_data::fail);
				puts_function(_held_messages.back());	
			}

			template<typename T>
			inline std::string do_got_msg(bool succ, T result)
			{
				if (succ)
				{
					return "";
				}
				return stringsprintf(_internal_got_msg,convert_to_string(result));
			}
		public:
			std::vector<std::string>& held_messages;
			std::vector<message_data>& message_metadata;

			inline void inform(std::string message)
			{
				_held_messages.push_back(std::string("\033[1;33m[INFO] ")+message);
				_message_metadata.push_back(message_data::info);
				puts_function(_held_messages.back());	
			}
			template<const char* format_string, typename ... T>
			void inform(T ... in)
			{
				inform(stringsprintf(format_string,in...));
			}
			inline void set_name(std::string new_name)
			{
				if (_name.has_value())
				{
					throw std::runtime_error("tried to set test name twice");
				}
				_name = {new_name};
				puts_function(get_name_msg());
			}
			std::string get_name_or_no_name()
			{
				return _name.has_value()? _name.value() : "NO NAME";
			}
			std::string get_name_msg()
			{
				return std::string("\033[1;35m[UNIT] " + get_name_or_no_name());
			}
			size_t get_fail_count()
			{
				size_t fail_count = 0;
				for (const message_data& data : _message_metadata)
				{
					if (data == message_data::fail)
					{
						fail_count++;
					}
				}
				return fail_count;
			}
			std::string get_end_status_msg()
			{
				auto fail_count = get_fail_count();
				return (fail_count == 0 ? "\033[1;42;37m[POST] ALL TESTS FOR UNIT PASS" : "\033[1;41;37m[POST] " + std::to_string(fail_count) + " TESTS FAILED")
					+ "\033[K\033[0m";
			}
			

		private: //Test implmentations
			template<class first_t, class second_t, class function_return_t, class wanted_t>
			bool _test(first_t const & first, std::string_view name_of_first, second_t const & second, std::string_view name_of_second, std::function<function_return_t()> func, std::string_view function_string, wanted_t _wanted)
			{
				auto wanted = static_cast<function_return_t>(_wanted);

				function_return_t result = func();
				bool result_is_pass = compare_and_convert_if_needed(result,wanted);

				std::string message = stringsprintf(_internal_test_format_2_variable,
					result_is_pass? _internal_pass_color : _internal_fail_color,
					std::string(result_is_pass? "PASS" : "FAIL"),
					std::string(function_string),
					std::string(name_of_first),
					escape_string(convert_to_string(first)),
					std::string(name_of_second),
					escape_string(convert_to_string(second)),
					escape_string(convert_to_string(wanted)),
					do_got_msg(result_is_pass,result)
				);

				push_back_test(message,result_is_pass);
				return result_is_pass;
			}
			template<class first_t, class function_return_t, class wanted_t>
			bool _test(first_t const & first, std::string_view name_of_first, std::function<function_return_t()> func, std::string_view function_string, wanted_t _wanted)
			{
				auto wanted = static_cast<function_return_t>(_wanted);

				function_return_t result = func();
				bool result_is_pass = compare_and_convert_if_needed(result,wanted);

				std::string message = stringsprintf(_internal_test_format_1_variable,
					result_is_pass? _internal_pass_color : _internal_fail_color,
					std::string(result_is_pass? "PASS" : "FAIL"),
					std::string(function_string),
					std::string(name_of_first),
					escape_string(convert_to_string(first)),
					escape_string(convert_to_string(wanted)),
					do_got_msg(result_is_pass,result)
				);

				push_back_test(message,result_is_pass);
				return result_is_pass;
			}
			template<class wanted_t, class function_return_t>
			bool _test(std::function<function_return_t(void)> func, std::string_view function_string, wanted_t _wanted)
			{
				auto wanted = static_cast<function_return_t>(_wanted);

				function_return_t result = func();
				bool result_is_pass = compare_and_convert_if_needed(result,wanted);

				std::string message = stringsprintf(_internal_test_format,
					result_is_pass? _internal_pass_color : _internal_fail_color,
					std::string(result_is_pass? "PASS" : "FAIL"),
					std::string(function_string),
					escape_string(convert_to_string(wanted)),
					do_got_msg(result_is_pass,result)
				);

				push_back_test(message,result_is_pass);
				return result_is_pass;
			}
		public: 
			/* 
			  These "wrappers" must exist because of the need for the following "_wanted" inputs to be valid:
					* Type that is convertible to the type returned by func
					* Type that is exactly the type returned by func BUT only knowable by looking at what exactly IS the type returned by func (e.g. finding that 
						func returns a std::optional<int> so that you know that {27} means std::optional<int>{27}
					* Type that is knowable without looking at the value returned by func (e.g. std::string("hi") )
			*/
			template<class first_t, class second_t, class function_return_t, class wanted_t>
			inline bool test(first_t const & first, std::string_view name_of_first, second_t const & second, std::string_view name_of_second, std::function<function_return_t()> func, std::string_view function_string, wanted_t _wanted)
			{
				return _test(first,name_of_first,second,name_of_second,func,function_string,_wanted);
			}
			template<class first_t, class second_t, class function_return_t>
			inline bool test(first_t const & first, std::string_view name_of_first, second_t const & second, std::string_view name_of_second, std::function<function_return_t()> func, std::string_view function_string, function_return_t _wanted)
			{
				return _test(first,name_of_first,second,name_of_second,func,function_string,_wanted);
			}



			template<class first_t, class function_return_t, class wanted_t>
			inline bool test(first_t const & first, std::string_view name_of_first, std::function<function_return_t()> func, std::string_view function_string, wanted_t _wanted)
			{
				return _test(first,name_of_first,func,function_string,_wanted);
			}
			template<class first_t, class function_return_t>
			inline bool test(first_t const & first, std::string_view name_of_first, std::function<function_return_t()> func, std::string_view function_string, function_return_t _wanted)
			{
				return _test(first,name_of_first,func,function_string,_wanted);
			}


			template<class wanted_t, class function_return_t>
			bool test(std::function<function_return_t(void)> func, std::string_view function_string, wanted_t _wanted)
			{
				return _test(func,function_string,_wanted);
			}
			template<class function_return_t>
			inline bool test(std::function<function_return_t(void)> func, std::string_view function_string, function_return_t _wanted)
			{
				return _test(func,function_string,_wanted);
			}

			instance() :
				_held_messages(),
				_message_metadata(),
				held_messages(_held_messages),
				message_metadata(_message_metadata)
			{
			}

			~instance() noexcept
			{
			}
			
			instance(const instance& other) noexcept :
				_held_messages(other._held_messages),
				_message_metadata(other._message_metadata),
				held_messages(this->_held_messages),
				message_metadata(this->_message_metadata)
			{
			}

			instance(instance&& other) noexcept : 
				_held_messages(std::move(other._held_messages)),
				_message_metadata(std::move(other._message_metadata)),
				held_messages(this->_held_messages),
				message_metadata(this->_message_metadata)
			{
			}


	};

	/*!
	 * Lambda captures [&] will capture this by reference in C++20. If your compiler does not support this, then you will have to work arround it.
	 */
	
#define TEST_2(first, second, operation, wanted) first , #first , second , #second ,std::function<decltype(operation)()>{[&]() { return (operation); }}, #operation , wanted
#define TEST_1(first, operation, wanted) first , #first ,std::function<decltype(operation)()>{[&]() { return (operation); }}, #operation , wanted
#define TEST_0(operation,wanted) std::function<decltype(operation)()>{[&](){return (operation); }}, #operation , wanted
	inline void _internal_run_tests(std::vector<instance>& instances, std::function<void(std::string)> puts_function, std::function<void(instance&)> this_test)
	{
		instances.emplace_back();
		auto& inst = instances.back();
		inst.puts_function = puts_function;
		try
		{
			this_test(inst);
		}
		catch (std::exception const & e)
		{
			auto message = std::string(_internal_fail_color) + "[CRIT] Test threw uncaught exception \""+std::string(e.what())+"\"";
			inst._held_messages.push_back(message);
			inst._message_metadata.push_back(instance::message_data::crit);
			puts_function(inst.held_messages.back());
		}

		/*puts_function(inst.get_name_msg());
		for(const std::string& msg : inst.held_messages)
		{
			puts_function(msg);
		}*/
		puts_function(inst.get_end_status_msg());
	}	

	template<typename ... tests_t>
	inline void _internal_run_tests(std::vector<instance>& instances, std::function<void(std::string)> puts_function, std::function<void(instance&)> this_test, tests_t ... tests)
	{
		_internal_run_tests(instances,puts_function,this_test);	
		_internal_run_tests(instances,puts_function,tests...);	
	}

	template<typename ... tests_t>
	inline std::vector<instance> run_tests(std::function<void(std::string)> puts_function, tests_t ... tests)
	{
		std::vector<instance> results = {};
		_internal_run_tests(results,puts_function,tests...);
		return results;
	}
	constexpr char _internal_log_format[] = "%s%";
	template<typename ... tests_t>
	inline std::vector<instance> run_tests(std::function<void(instance&)> first_test, tests_t ... tests)
	{
		return run_tests([](std::string msg)
		{
			puts(msg.c_str());
			logger::unit_test_log<_internal_log_format>(msg);
		},
		first_test, tests...);
	}



	void testtest();


}
