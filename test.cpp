//Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
#include "test.hpp"
#include <cstdio>

//tests for the test library

using namespace test;

class ComparableAfterConvert_B;
class ComparableAfterConvert_A
{
	public:

		int val;
		ComparableAfterConvert_A(int val)
		{
			this->val = val;
		}
		explicit ComparableAfterConvert_A( const ComparableAfterConvert_B& other);
		ComparableAfterConvert_A& operator=(const ComparableAfterConvert_B& other);
};
class ComparableAfterConvert_B
{
	public:
		ComparableAfterConvert_B(int val)
		{
			this->val = val;
		}

		int val;
};
ComparableAfterConvert_A::ComparableAfterConvert_A( const ComparableAfterConvert_B& other)
{
	this->val = other.val;	
}
ComparableAfterConvert_A& ComparableAfterConvert_A::operator=(const ComparableAfterConvert_B& other)
{
	this->val = other.val;
	return *this;
}
bool operator==(const ComparableAfterConvert_A& a, const ComparableAfterConvert_A& b)
{
	return a.val == b.val;
}

std::string convert_to_string(ComparableAfterConvert_A data) { return std::to_string(data.val); }
std::string convert_to_string(ComparableAfterConvert_B data) { return std::to_string(data.val); }

void _test(test::instance& inst)
{

	using namespace std::string_literals;
	//This library has to be tested by hand, since using a unit test library to test itself would be a catch 22 if it had any errors in the first place.
	inst.set_name("Unit test library");
	int a =76;
	int b = 2;

	inst.inform("The first test should fail and at the end an uncaught exception will be thrown; the rest should not.");

	inst.test(TEST_2(a,b,a==b,true));
	inst.test(TEST_1(a,a==76,true));
	inst.test(TEST_0(76==76,true));

	inst.test(TEST_0(76,76));
	
	ComparableAfterConvert_B B = {27};

	ComparableAfterConvert_A A(B);

	static_cast<ComparableAfterConvert_A>(B);

	ComparableAfterConvert_A one(666);
	ComparableAfterConvert_A two(666);
	//printf("1?%d\n",one == two);

	ComparableAfterConvert_A my_a(666);
	ComparableAfterConvert_B my_b(666);

	inst.test(TEST_0(ComparableAfterConvert_A(666),my_b));
	inst.test(TEST_0(my_a,my_b));

	inst.test(TEST_2(my_a,my_a,test::compare_and_convert_if_needed(my_a,my_b),true));

	inst.inform("Testing escape_string");

	inst.test(TEST_0(escape_string("regular \0 \n text?"s),"regular [NULL] [NL] text?"s));
	inst.test(TEST_0(escape_string("regular \0 \n"s),"regular [NULL] [NL]"s));
	inst.test(TEST_0(escape_string("regular text?"s),"regular text?"s));
	inst.test(TEST_0(escape_string("regular \" text?"s),"regular \\\" text?"s));


	std::string strtest = "str test";

	printf("%s\n",convert_to_string(strtest).c_str());

	throw std::runtime_error("This exception should be shown as uncaught by the testing library");
}
void test::testtest()
{
	test::run_tests(_test);
}
