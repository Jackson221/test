/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Forward declares test.hpp contents to avoid needing to include definitions in header files
 *
 * A necessary evil for improved compiling speeds and avoiding circular dependancies
 *
 * Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

namespace test
{
	class instance;
}
